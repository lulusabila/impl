import java.awt.*;         
import javax.swing.*;
import java.awt.event.*;

public class ContohPoligon extends JFrame {
   public static void main(String[] args) {
      ContohPoligon apl =
         new ContohPoligon();
   }

   public ContohPoligon() {
      super("Contoh Poligon");  // nama yang kita buat pertama kali
      setSize(450, 300);           // ukuran form yang akan kita buat terserah mau segedek mana anda buat
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      CanvasKu kanvas = new CanvasKu();
      add(kanvas);

      show();
   }
}

